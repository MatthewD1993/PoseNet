import tensorflow as tf
from tensorflow.python.ops.metrics import mean, root_mean_squared_error
import math


def average_position_error(labels, predictions):
    """
    Average positional displacement from ground truth position.
    :param labels: Assume labels dimension. [batch_size, 3]
    :param predictions:
    :return:
    """
    # return root_mean_squared_error(labels, predictions)
    return tf.sqrt(tf.reduce_mean(tf.square(labels-predictions)))


def average_angular_error(labels, predictions):
    """
    :param labels:
    :param predictions:
    :return: Average angular loss in degrees.
    """
    q1 = labels / tf.norm(labels, axis=1, keep_dims=True)
    q2 = predictions / tf.norm(predictions, axis=1, keep_dims=True)
    d = tf.abs(tf.reduce_sum(tf.multiply(q1, q2), axis=1))
    theta = 2 * tf.acos(d) * 180 / math.pi
    return tf.reduce_mean(theta)