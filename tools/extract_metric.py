import tensorflow as tf
import argparse
import os
parser = argparse.ArgumentParser()
parser.add_argument('target', type=str, help='The target metric to extract.')
parser.add_argument('event_file_path', type=str, help='The event file path.')

args = parser.parse_args()
target = args.target
event_file_path = args.event_file_path

cwd = os.getcwd()

file_name = os.path.join(cwd, target.split('/')[-1]+'.txt')
values = []
with open(file_name, 'w') as f:
    for e in tf.train.summary_iterator(event_file_path):
        for v in e.summary.value:
            if v.tag == target:
                f.write(str(v.simple_value)+'\n')
