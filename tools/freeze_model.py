import argparse
import os

import tensorflow as tf

import freeze_graph as fg

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = '3'

parser = argparse.ArgumentParser()
parser.add_argument('ckpt_path', type=str, help='The path to saved ckpt file.')
args = parser.parse_args()
ckpt_path = args.ckpt_path

# print(model_folder)
if os.path.isdir(ckpt_path):
    # We retrieve our checkpoint fullpath
    checkpoint = tf.train.get_checkpoint_state(ckpt_path)
    input_checkpoint = checkpoint.model_checkpoint_path

else:
    input_checkpoint = ckpt_path
# Before exporting our graph, we need to precise what is our output node
# This is how TF decides what part of the Graph he has to keep and what part it can dump
# NOTE: this variable is plural, because you can have multiple output nodes
# output_node_names = "final_pose"
output_node_names = "cls3_fc_pose_xyz/cls3_fc_pose_xyz,cls3_fc_pose_wpqr/cls3_fc_pose_wpqr"

fg.freeze_graph(input_checkpoint, output_node_names=output_node_names)
