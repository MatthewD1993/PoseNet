import sys
import os
import numpy as np
POSE_TXT_PATH = sys.argv[1]
POSE_TXT_PATH = os.path.abspath(POSE_TXT_PATH)

dataset_path = os.path.dirname(POSE_TXT_PATH)
dataset_path = os.path.dirname(dataset_path)
train_txt_path = os.path.join(dataset_path, 'train.txt')
test_txt_path  = os.path.join(dataset_path, 'test.txt')
print(train_txt_path)

split_ratio = 0.9
train_file = open(train_txt_path, 'wb')
test_file  = open(test_txt_path, 'wb')

with open(POSE_TXT_PATH,'rb') as f:
    lines = f.readlines()
    np.random.shuffle(lines)
    total = len(lines)
    train_n = int(total*split_ratio)
    test_n = total - train_n
    print('Total number of images: ', total)
    print('Split ratio: ', split_ratio)
    print('Train: ', train_n)
    print('Test: ', test_n)
    for l in range(train_n):
        train_file.write(lines[l])
    for l in range(train_n, total):
        test_file.write(lines[l])

train_file.close()
test_file.close()