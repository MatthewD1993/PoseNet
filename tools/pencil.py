import cv2
import numpy as np


def create_pencil_image(input_image, dilate_size = 2):

    input_image = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)

    element = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (2 * dilate_size + 1, 2 * dilate_size + 1),
                                        (dilate_size, dilate_size))

    pencil_image = cv2.dilate(input_image, element)

    valorig = input_image.copy()
    valmax = pencil_image.copy()
    valout = pencil_image.copy()
    alpha_image = pencil_image.copy()

    valout[valmax == 0] = 255

    alpha_image[valmax > 0] = (valorig[valmax>0]*float(255.0)) / valmax[valmax > 0]

    valout[valout < 255] = alpha_image[valout < 255]

    pencil_image = valout.copy()
    pencil_image = np.expand_dims(pencil_image, axis=2)

    return pencil_image
