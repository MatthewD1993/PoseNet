import tensorflow as tf


# helper functions
def diag(a, b):  # computes the diagonal entries,  1 - 2*a**2 - 2*b**2
    return 1 - 2 * tf.pow(a, 2) - 2 * tf.pow(b, 2)


def tr_add(a, b, c, d):  # computes triangle entries with addition
    return 2 * a * b + 2 * c * d


def tr_sub(a, b, c, d):  # computes triangle entries with subtraction
    return 2 * a * b - 2 * c * d


def q2R(pred_qs, name=None):
    with tf.name_scope(name, "q2R", [pred_qs]):

        norms = tf.sqrt(tf.reduce_sum(tf.square(pred_qs), axis=1, keep_dims=True))
        normed_qs = tf.divide(pred_qs, norms)

        w, x, y, z = tf.unstack(normed_qs, axis=1)
        R00 = diag(y, z)
        R01 = tr_sub(x, y, z, w)
        R02 = tr_add(x, z, y, w)
        R10 = tr_add(x, y, z, w)
        R11 = diag(x, z)
        R12 = tr_sub(y, z, x, w)
        R20 = tr_sub(x, z, y, w)
        R21 = tr_add(y, z, x, w)
        R22 = diag(x, y)

        R0 = tf.stack([R00, R01, R02])
        R1 = tf.stack([R10, R11, R12])
        R2 = tf.stack([R20, R21, R22])
        R = tf.stack([R0, R1, R2])

        R = tf.transpose(R, perm=[2, 0, 1])
        return R


def transform(R_e, t, vertices, n, batch_size=None):
    """
    Transform vertices coordinates with predicted R
    :param R_e: shape [batch_size, 3, 3]
    :param t:   shape [batch_size, 3, 1]
    :param vertices: should have shape [3, n], n is the number of vertices
    :return:
    """

    return tf.reshape(tf.matmul(tf.reshape(R_e, [-1, 3]), vertices), [-1, 3, n]) + t


def new_loss(real_v_p, pred_v_p):
    """
    L2 distance.
    :param pred_v_p: shape [batch_size, 3, n]
    :param real_v_p: shape [batch_szie, 3, n]
    :return:
    """
    return tf.losses.mean_squared_error(real_v_p, pred_v_p)