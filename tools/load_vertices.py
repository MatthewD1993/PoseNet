import numpy as np
# from pyquaternion import Quaternion
# import tensorflow as tf
# import tfquaternion as tfq


def load_vertices(filename):

    vertices = []
    with open(filename) as f:
        for line in f:
            all = line.split()
            if all:
                if all[0] == 'v':
                    x = float(all[1])
                    y = float(all[2])
                    z = float(all[3])
                    vertices.append((x, y, z))

    print('Load {} vertices.'.format(np.size(vertices,0)))
    return vertices


def select_random_vertices_set(all_vertices, number_of_rand=None, use_all=False):
    if use_all:
        print('Number of total vertices: ', len(all_vertices))
        return all_vertices, len(all_vertices)
    else:
        all_vertices = np.array(all_vertices)
        select_vertices = np.random.shuffle(all_vertices)[:number_of_rand]
        return select_vertices, number_of_rand


