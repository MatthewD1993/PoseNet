"""
New loss: mean square error of predicted vertices coordinates over each dimension. L2
pos_loss_rms: Absolute average disp of predicted position over each dimension. L1
"""
import argparse
import os

import numpy as np
import tensorflow as tf

from data_loader import DataSource
from posenet import GoogLeNet as PoseNet
from tools.load_vertices import load_vertices
from tools.load_vertices import select_random_vertices_set
from tools.metrics import average_position_error, average_angular_error
from tools.transform import q2R, transform, new_loss

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = '0'


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--log_path', type=str, default='./log/', help='The path to store training log and checkpoint files.')
    parser.add_argument('-d', '--data_type', choices=['RGB', 'Pencil', 'Both'], default='Pencil',
                        help='The data type that be used for training.')
    parser.add_argument('-s', '--sub_mean', default=False, action='store_true',
                        help='Switch to subtract the mean.')
    parser.add_argument('--root_dir', default='./data')
    parser.add_argument('--img_size', type=int, nargs='+', default=[448, 448])
    parser.add_argument('--mode', choices=['train', 'val'], default='train')
    parser.add_argument('--resume', default=False, action='store_true')
    # parser.add_argument('--ckpt_path', type=str, default=None)
    parser.add_argument('--ckpt_path', type=str, default='/cdengdata/posenet/iron_aug_old_loss_Pencil/PoseNet.ckpt_49999_27.533')
    parser.add_argument('--batch_size', type=int, default=30)
    parser.add_argument('--max_iteration', type=int, default=50000)
    parser.add_argument('--obj_path', type=str, default='./data/iron.obj')
    args = parser.parse_args()

    object_model = args.obj_path
    batch_size = args.batch_size
    log_path = args.log_path
    max_iteration = args.max_iteration

    use_all = True
    n = 100

    vertices_set = load_vertices(filename=object_model)
    sample_vertices, n = select_random_vertices_set(all_vertices=vertices_set, number_of_rand=n, use_all=use_all)

    # Mesh obj is in mm, while pose [x, y, z] is in cm. Divide by 10 to keep them consistent.
    sample_vertices = np.float32(np.transpose(sample_vertices))/10
    # sample_vertices = np.float32(np.transpose(sample_vertices))
    print('sample vertices shape: ', sample_vertices.shape)

    train_data = DataSource(args, text_file='small_test.txt')
    test_data  = DataSource(args, text_file='small_test.txt')

    channel_dict = {'Both': 4, 'Pencil': 1, 'RGB': 3}
    in_shape = [None] + args.img_size + [channel_dict[args.data_type]]
    images   = tf.placeholder(tf.float32, in_shape)
    init_vertices = tf.placeholder(tf.float32, [3, None])
    poses_x = tf.placeholder(tf.float32, [None, 3])
    poses_q = tf.placeholder(tf.float32, [None, 4])

    net = PoseNet({'data': images})

    p1_x = net.layers['cls1_fc_pose_xyz']
    p1_q = net.layers['cls1_fc_pose_wpqr']
    p2_x = net.layers['cls2_fc_pose_xyz']
    p2_q = net.layers['cls2_fc_pose_wpqr']
    p3_x = net.layers['cls3_fc_pose_xyz']
    p3_q = net.layers['cls3_fc_pose_wpqr']

    with tf.name_scope("new_loss") as scope:
        R = q2R(poses_q, name=scope)
        t = tf.expand_dims(poses_x, axis=2)
        gt_vertices = transform(R, t, init_vertices, n, batch_size)

        losses = []

        R_p_1 = q2R(p1_q)
        t_p_1 = tf.expand_dims(p1_x, dim=2)
        predicted_vertices_1 = transform(R_p_1, t_p_1, init_vertices, n, batch_size)
        loss_1 = new_loss(gt_vertices, predicted_vertices_1)
        losses.append(loss_1)
        pos_loss_1 = average_position_error(poses_x, p1_x)
        ang_loss_1 = average_angular_error(poses_q, p1_q)
        tf.summary.scalar('new_loss_1', loss_1)
        tf.summary.scalar('pos_loss_rms_1', pos_loss_1)
        tf.summary.scalar('ang_loss_degree_1', ang_loss_1)

        R_p_2 = q2R(p2_q)
        t_p_2 = tf.expand_dims(p2_x, dim=2)
        predicted_vertices_2 = transform(R_p_2, t_p_2, init_vertices, n, batch_size)
        loss_2 = new_loss(gt_vertices, predicted_vertices_2)
        losses.append(loss_2)
        pos_loss_2 = average_position_error(poses_x, p2_x)
        ang_loss_2 = average_angular_error(poses_q, p2_q)
        tf.summary.scalar('new_loss_2', loss_2)
        tf.summary.scalar('pos_loss_rms_2', pos_loss_2)
        tf.summary.scalar('ang_loss_degree_2', ang_loss_2)

        R_p_3 = q2R(p3_q)
        t_p_3 = tf.expand_dims(p3_x, dim=2)
        predicted_vertices = transform(R_p_3, t_p_3, init_vertices, n, batch_size)
        loss_3 = new_loss(gt_vertices, predicted_vertices)
        losses.append(loss_3)
        pos_loss_3 = average_position_error(poses_x, p3_x)
        ang_loss_3 = average_angular_error(poses_q, p3_q)
        tf.summary.scalar('new_loss_3', loss_3)
        tf.summary.scalar('pos_loss_rms_3', pos_loss_3)
        tf.summary.scalar('ang_loss_degree_3', ang_loss_3)

        loss = tf.losses.compute_weighted_loss(losses, [10, 10, 30])
        tf.summary.scalar('new_loss', loss)

    # pos_loss = average_position_error(poses_x, p3_x)
    # ang_loss = average_angular_error(poses_q, p3_q)
    # tf.summary.scalar('pos_loss_rms', pos_loss)
    # tf.summary.scalar('ang_loss_degree', ang_loss)

    merged = tf.summary.merge_all()
    # validation_merged = tf.summary.merge(['pos_loss', 'ang_loss'])

    opt = tf.train.AdamOptimizer(learning_rate=0.0001, beta1=0.9, beta2=0.999, epsilon=0.00000001, use_locking=False,
                                 name='Adam').minimize(loss)

    # Set GPU options
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.95)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver()
    outputFile = os.path.join(log_path, "PoseNet.ckpt")

    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
        train_writer = tf.summary.FileWriter(os.path.join(log_path, 'train'), sess.graph)
        test_writer = tf.summary.FileWriter(os.path.join(log_path, 'test'), sess.graph)

        # Load the data
        sess.run(init)
        if args.resume:
            try:
                saver.restore(sess, args.ckpt_file)
            except:
                print('>>> Warning: Please check ckpt path correctness.')

        for i in range(max_iteration):

            np_images, np_poses_x, np_poses_q = next(train_data.batch_gen)
            feed = {images: np_images, init_vertices: sample_vertices, poses_x: np_poses_x, poses_q: np_poses_q}

            np_loss, _, summary = sess.run([loss, opt, merged], feed_dict=feed)
            train_writer.add_summary(summary, i)

            # Validation
            if (i+1) % 10 == 0:
                if (i+1) % 1000 == 0:
                    saved_path = outputFile + '_' + str(i) + '_' + str(np_loss)
                    saver.save(sess, saved_path)
                    print("Intermediate file saved at: " + saved_path)

                print("Start validating at iteration {}!".format(i))
                val_loss = []
                val_pos_loss = []
                val_ang_loss = []
                for np_image, np_pose_x, np_pose_q in test_data:
                    feed = {images: [np_image], init_vertices: sample_vertices, poses_x: [np_pose_x], poses_q: [np_pose_q]}
                    val_step_loss, val_step_pos, val_step_ang = sess.run([loss, pos_loss_3, ang_loss_3], feed_dict=feed)
                    val_loss.append(val_step_loss)
                    val_pos_loss.append(val_step_pos)
                    val_ang_loss.append(val_step_ang)

                val_mean_loss, val_mean_pos, val_mean_ang = np.mean(val_loss), np.mean(val_pos_loss), np.mean(val_ang_loss)
                test_summary = tf.Summary()
                test_summary.value.add(tag='final_loss_mse', simple_value=val_mean_loss)
                test_summary.value.add(tag='pos_loss_rms', simple_value=val_mean_pos)
                test_summary.value.add(tag='ang_loss_degree', simple_value=val_mean_ang)

                # test_summary = sess.run(merged, feed_dict=feed)
                test_writer.add_summary(test_summary, i)


if __name__ == '__main__':
    main()





