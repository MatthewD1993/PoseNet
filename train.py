import argparse
import os

import numpy as np
import tensorflow as tf

from data_loader import DataSource
from posenet import GoogLeNet as PoseNet
from tools.metrics import average_angular_error

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = '3'


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--log_path', type=str, default='./log/', help='The path to store training log and checkpoint files.')
    parser.add_argument('-d', '--data_type', choices=['RGB', 'Pencil', 'Both'], default='Pencil',
                        help='The data type that be used for training.')
    parser.add_argument('-s', '--sub_mean', default=False, action='store_true',
                        help='Switch to subtract the mean.')

    parser.add_argument('--root_dir', default='./data')
    parser.add_argument('--img_size', type=int, nargs='+', default=[448, 448])

    parser.add_argument('--mode', choices=['train', 'val'], default='train')
    parser.add_argument('--resume', default=False, action='store_true')

    # parser.add_argument('--ckpt_path', type=str, default=None)
    parser.add_argument('--ckpt_path', type=str, default='/cdengdata/posenet/iron_aug_old_loss_Pencil/PoseNet.ckpt_49999_27.533')

    parser.add_argument('--batch_size', type=int, default=30)
    parser.add_argument('--max_iteration', type=int, default=50000)

    args = parser.parse_args()

    log_path = args.log_path
    resume   = args.resume
    ckpt_file = args.ckpt_path
    max_iteration = args.max_iteration

    train_data = DataSource(args, text_file='train.txt')
    test_data  = DataSource(args, text_file='test.txt')

    channel_dict = {'Both': 4, 'Pencil': 1, 'RGB': 3}
    in_shape = [None] + args.img_size + [channel_dict[args.data_type]]

    images  = tf.placeholder(tf.float32, in_shape)
    poses_x = tf.placeholder(tf.float32, [None, 3])
    poses_q = tf.placeholder(tf.float32, [None, 4])

    net = PoseNet({'data': images})

    p1_x = net.layers['cls1_fc_pose_xyz']
    p1_q = net.layers['cls1_fc_pose_wpqr']
    p2_x = net.layers['cls2_fc_pose_xyz']
    p2_q = net.layers['cls2_fc_pose_wpqr']
    p3_x = net.layers['cls3_fc_pose_xyz']
    p3_q = net.layers['cls3_fc_pose_wpqr']

    with tf.name_scope("old_loss") as scope:
        losses = []
        l1_x = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(p1_x, poses_x))))
        losses.append(l1_x)
        l1_q = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(p1_q, poses_q))))
        losses.append(l1_q)
        l2_x = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(p2_x, poses_x))))
        losses.append(l2_x)
        l2_q = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(p2_q, poses_q))))
        losses.append(l2_q)
        l3_x = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(p3_x, poses_x))))
        losses.append(l3_x)
        l3_q = tf.sqrt(tf.reduce_mean(tf.square(tf.subtract(p3_q, poses_q))))
        losses.append(l3_q)

        loss = tf.losses.compute_weighted_loss(losses, [10, 300, 10, 300, 30, 900])

        ang_loss_1 = average_angular_error(poses_q, p1_q)
        ang_loss_2 = average_angular_error(poses_q, p2_q)
        ang_loss_3 = average_angular_error(poses_q, p3_q)

        tf.summary.scalar('pos_loss_rms_1', l1_x)
        tf.summary.scalar('ang_loss_degree_1', ang_loss_1)

        tf.summary.scalar('pos_loss_rms_2', l2_x)
        tf.summary.scalar('ang_loss_degree_2', ang_loss_2)

        tf.summary.scalar('pos_loss_rms_3', l3_x)
        tf.summary.scalar('ang_loss_degree_3', ang_loss_3)

        # loss = tf.losses.get_total_loss()
        tf.summary.scalar('loss', loss)

    merged = tf.summary.merge_all()
    # validation_merged = tf.summary.merge(['pos_loss', 'ang_loss'])

    opt = tf.train.AdamOptimizer(learning_rate=0.0001, beta1=0.9, beta2=0.999, epsilon=0.00000001, use_locking=False,
                                 name='Adam').minimize(loss)

    # Set GPU options
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.95)

    init = tf.global_variables_initializer()
    saver = tf.train.Saver(max_to_keep=3)
    outputFile = os.path.join(log_path, "PoseNet.ckpt")

    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
        train_writer = tf.summary.FileWriter(os.path.join(log_path, 'train'), sess.graph)
        test_writer = tf.summary.FileWriter(os.path.join(log_path, 'test'), sess.graph)

        # Load the data
        sess.run(init)
        if resume:
            try:
                saver.restore(sess, ckpt_file)
            except:
                print('>>> Error: Loading checkpoint failed..')

        for i in range(max_iteration):

            np_images, np_poses_x, np_poses_q = next(train_data.batch_gen)

            feed = {images: np_images, poses_x: np_poses_x, poses_q: np_poses_q}

            # print("iteration: " + str(i) + "\n\t" + "Loss is: " + str(np_loss))
            np_loss, _, summary = sess.run([loss, opt, merged], feed_dict=feed)
            train_writer.add_summary(summary, i)

            # Validation
            if (i+1) % 10 == 0:

                if (i+1) % 1000 == 0:
                    saved_path = outputFile + '_' + str(i) + '_' + str(np_loss)
                    saver.save(sess, saved_path)
                    print("Intermediate file saved at: " + saved_path)

                print("Start validating at iteration {}!".format(i))
                # test_data_gen = gen_data(test_data_source, train=False)
                val_loss = []
                val_pos_loss = []
                val_ang_loss = []
                for np_image, np_pose_x, np_pose_q in test_data:
                    feed = {images: [np_image], poses_x: [np_pose_x], poses_q: [np_pose_q]}
                    val_step_loss, val_step_pos, val_step_ang = sess.run([loss, l3_x, ang_loss_3], feed_dict=feed)
                    val_loss.append(val_step_loss)
                    val_pos_loss.append(val_step_pos)
                    val_ang_loss.append(val_step_ang)

                val_mean_loss, val_mean_pos, val_mean_ang = np.mean(val_loss), np.mean(val_pos_loss), np.mean(val_ang_loss)
                test_summary = tf.Summary()
                test_summary.value.add(tag='final_loss_mse', simple_value=val_mean_loss)
                test_summary.value.add(tag='pos_loss_rms', simple_value=val_mean_pos)
                test_summary.value.add(tag='ang_loss_degree', simple_value=val_mean_ang)

                # test_summary = sess.run(merged, feed_dict=feed)
                test_writer.add_summary(test_summary, i)


if __name__ == '__main__':
    main()
