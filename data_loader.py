import os

import cv2
import numpy as np
from tqdm import tqdm

from tools.pencil import create_pencil_image


class DataSource(object):
    def __init__(self, data_args, text_file='train.txt'):
        image_paths, poses = self.read_txt(os.path.join(data_args.root_dir, text_file), root_dir=data_args.root_dir)
        image_base = self.load_image(image_paths, img_size=data_args.img_size, data_type=data_args.data_type,
                                     sub_mean=data_args.sub_mean)
        self.images = image_base
        self.poses = poses
        self.size = len(image_base)

        self.batch_size = data_args.batch_size
        self.current = 0

    def __len__(self):
        return self.size

    @staticmethod
    def load_image(img_paths, img_size=(448, 448), data_type='RGB', sub_mean=False):
        """
        Load images using given img_paths, and process the images by configuration.
        :param img_paths:
        :param img_size: The shape of required image size. (448, 448)
        :param data_type: One of {'RGB', 'Pencil', 'Both'}
        :param sub_mean: Whether to subtract the mean by channel.
        :return: Processed image dataset in a list.
        """
        img = cv2.imread(img_paths[0])

        assert img.shape[:2] != img_size, 'The image shape {} does not fit shape:{} '.format(img.shape, img_size)
        image_base = []
        for i in tqdm(range(len(img_paths))):
            x = cv2.imread(img_paths[i])
            # pencil filter should be one channel output
            if data_type == 'RGB':
                data = x
            elif data_type == 'Pencil':
                data = create_pencil_image(x)
            elif data_type == 'Both':
                pencil_img = create_pencil_image(x)
                data = np.concatenate((x, pencil_img), axis=2)
            else:
                raise ValueError("data type should be one of:'RGB', 'Pencil', 'Both' ")
            image_base.append(data)

        print('Subtract mean: ', sub_mean)
        # Subtract the mean of the image by channel.
        if sub_mean:
            image_base = [x-np.mean(x, axis=(0, 1), keepdims=True) for x in image_base]

        return np.array(image_base)

    @staticmethod
    def read_txt(txt_path, root_dir=None, shuffle=True):
        poses = []
        image_paths = []
        print('Shuffle the data: ', shuffle)

        with open(txt_path) as f:
            lines = f.readlines()
            np.random.shuffle(lines) if shuffle else None
            for line in lines:
                elements = line.split()
                image_paths.append(os.path.join(root_dir, elements[0]))
                poses.append([float(p) for p in elements[1:]])

        return np.array(image_paths), np.array(poses)

    def __iter__(self):
        return self

    def next(self):
        if self.current >= self.size:
            self.current = 0
            raise StopIteration
        else:
            self.current += 1
            return self.images[self.current-1], self.poses[self.current-1][:3], self.poses[self.current-1][3:]

    @property
    def batch_gen(self):
        while True:
            index = np.random.choice(self.size, self.batch_size, replace=False)
            yield self.images[index], self.poses[index, :3], self.poses[index, 3:]