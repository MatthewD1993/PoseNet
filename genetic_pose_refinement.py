import numpy as np
import imageio
import tensorflow as tf

from posenet import GoogLeNet as PoseNet
from deeptracking.data.modelrenderer import ModelRenderer, InitOpenGL
from deeptracking.utils.camera import Camera
from deeptracking.utils.transform import Transform


FLOAT_EPS = np.finfo(np.float).eps


def quat2mat(q):
    ''' Calculate rotation matrix corresponding to quaternion

    Parameters
    ----------
    q : 4 element array-like

    Returns
    -------
    M : (3,3) array
      Rotation matrix corresponding to input quaternion *q*

    Notes
    -----
    Rotation matrix applies to column vectors, and is applied to the
    left of coordinate vectors.  The algorithm here allows non-unit
    quaternions.

    References
    ----------
    Algorithm from
    https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion

    Examples
    --------
    >>> import numpy as np
    >>> M = quat2mat([1, 0, 0, 0]) # Identity quaternion
    >>> np.allclose(M, np.eye(3))
    True
    >>> M = quat2mat([0, 1, 0, 0]) # 180 degree rotn around axis 0
    >>> np.allclose(M, np.diag([1, -1, -1]))
    True
    '''
    w, x, y, z = q
    Nq = w * w + x * x + y * y + z * z
    if Nq < FLOAT_EPS:
        return np.eye(3)
    s = 2.0 / Nq
    X = x * s
    Y = y * s
    Z = z * s
    wX, wY, wZ = w * X, w * Y, w * Z
    xX, xY, xZ = x * X, x * Y, x * Z
    yY, yZ, zZ = y * Y, y * Z, z * Z
    return np.array([[1.0 - (yY + zZ), xY - wZ, xZ + wY],
                     [xY + wZ, 1.0 - (xX + zZ), yZ - wX],
                     [xZ - wY, yZ + wX, 1.0 - (xX + yY)]])


class PencilPosenetEstimator(object):
    def __init__(self, weigths_path, sess):
        self.images = tf.placeholder(tf.float32, [None, 448, 448, 4])

        network = PoseNet({'data': self.images})

        self.p3_x = network.layers['cls3_fc_pose_xyz']
        self.p3_q = network.layers['cls3_fc_pose_wpqr']

        self.saver = tf.train.Saver()
        self.sess = sess
        self.saver.restore(sess, weigths_path)

    def forward(self, images):
        if images.ndims == 3:
            images = np.expand_dims(images, axis=0)
        p3_x, p3_q = self.sess.run([self.p3_x, self.p3_q], feed_dict={self.images:images})
        return np.concatenate((p3_x, p3_q))

    def __call__(self, *args, **kwargs):
        self.forward(*args, **kwargs)


class GeneticPoseEstimation(object):
    """
    1. Initialize: 3D model,
    2. Render (in: pose --> out: image)
    """
    def __init__(self, estimator, model3d_path):
        camera_path = './deeptracking/data/sensors/camera_parameter_files/synthetic.json'
        shader_path = './deeptracking/data/shaders/'
        camera = Camera.load_from_json(camera_path)
        window_size = [640, 480]
        window = InitOpenGL(*window_size)
        self.estimator = estimator
        self.render = ModelRenderer(model3d_path, shader_path=shader_path, camera=camera, window=window,
                                    window_size=window_size)

    def evolve(self, image, num_iter=1):
        est_pose = list()
        est_pose.append(self.estimator(image))
        for i in range(num_iter):
            est_pose.append(self.refine_iter(image, est_pose))
        return est_pose[-1]

    def refine_iter(self, image, pose):
        candidates = self.generate_candidates(pose)
        fitness_values = self.fitness(image, candidates)
        survivor = self.selection(candidates, fitness_values)
        return survivor

    def generate_candidate(self, pose):
        return None

    def selection(self, candidates, fitness_values):
        # TODO: roulette wheel
        return None

    def fitness(self, image, candidate):
        # TODO: adapt for multi candidates
        trans = Transform()
        trans.set_translation(*candidate[:3])
        trans.matrix[0:3, 0:3] = quat2mat(candidate[3:])
        render_rgb, _ = self.render(trans)
        res = np.sum(np.square(image-render_rgb))
        return res


def main():
    posenet_weights = '/path/to/weights'
    model3d_path = './3dmodels/iron_good.ply'

    sample_image_path = '/path/to/img'
    sample_image = imageio.imread(sample_image_path)
    num_iter = 2

    with tf.Session as sess:
        estimator = PencilPosenetEstimator(posenet_weights, sess)
        ga_refiner = GeneticPoseEstimation(estimator, model3d_path)
        refined_pose = ga_refiner.evolve(sample_image, num_iter)

    print('>>> Final pose: ', refined_pose)


if __name__ == '__main__':
    main()