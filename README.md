# This is the implementation of the paper:
Learning 6DoF Object Poses from Synthetic Single Channel Images 



## Highlights 
1. In this paper, we disregard all depth and color information and train a CNN to directly regress 6DoF object poses using only synthetic single channel edge enhanced images.
2. This implementation also includes the MSE between predicted 3D coordinates of thousands of vertice and groundtrut coordinates.

## Installation
This repository requires following packages:
- [Python](https://www.python.org/) >= 2.7 
- [Numpy](https://pypi.python.org/pypi/numpy) >= 1.12.1
- [OpenCV](https://opencv.org/) >= 2.4.13
- [TensorFlow](https://www.tensorflow.org/install/) = 1.2.1
- [tqdm](https://github.com/tqdm/tqdm)


## How to train
#### Train with MSE loss of the predicted 7D pose
```bash
python train.py --root_dir /root_dir_to_data_set/ 
```

#### Train with MSE loss of thousands of predicted spatial coordinates of CAD model vertice.
```bash
python add_train.py --root_dir /root_dir_to_data_set/ 
```
Both of this two script suport 3 input data type ['RGB', 'Pencil', 'Both'], and also mean substraction.

**Flags**: 
- `log_path`: The direactory path to the store the log files.
- `data_type`: The type of the images for training. One of: ['RGB', 'Pencil', 'Both']
- `sub_mean`: Switch to subtract the mean of each channel in each image.
- `root_dir`: The root directory of dataset.
- `img_size`: The image size for training: Default: [448, 448]
- `mode`: One of: ['train', 'test' ]
- `resume`: Switch to resume from trained weight.
- `ckpt_path`: The path to checkpoint file for restoring.
- `batch_size`: Batch size. Default: 30
- `max_iteration`: Max training iteration. Default: 50000


## How to test (requires training first)
#### Test to see the performance of our model.
```bash
python test.py --root_dir /path_to_dataset/ --text_file small_text.txt 
```

**Flags**: 
- `log_path`: The direactory path to the store the log files.
- `data_type`: The type of the images for training. One of: ['RGB', 'Pencil', 'Both']
- `sub_mean`: Switch to subtract the mean of each channel in each image.
- `root_dir`: The root directory of dataset.
- `text_file`: The text file that list all the images and groudtruth poses.
- `img_size`: The image size for training: Default: [448, 448]
- `ckpt_path`: The path to checkpoint file for restoring.
- `batch_size`: Batch size. Default: 30


## Results


## Dataset Organization
The data set should be organized in the following structure:
```
-- data_folder
    -- image_folder
        -- image.png
    -- train.txt
    -- text.txt
```

While text file contains entries like:
```
image_folder/image_00010739.png -16.3134 9.17614 99.2389 0.0627661 -0.165532 -0.422602 -0.888857

```


## Auxiliary Tools
In `tools` folder, we also include the tool for:
1. Export TensorFlow model and weights to a format that can be loaded with OpenCV (>3.3):
```
python freeze_model.py --ckpt_path /the_path_to_ckpt_file/ --output_node node_name_in_tf_graph
```
    **Flags**: 
    - `ckpt_path`: The path to checkpoint file for restoring.
    - `output_node`: The node name in TensorFlow graph, structure between inputs and output node is exported.

2. Extracting training log from *.tfevents file.
```
python extract_metric.py old_loss/loss /path_to_tfevents_file/
```