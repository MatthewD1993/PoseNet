import numpy as np
import tensorflow as tf
from posenet import GoogLeNet as PoseNet
import math
import argparse
import os
from data_loader import DataSource

os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"] = "0"


def main():

    parser = argparse.ArgumentParser()
    parser.add_argument('--log_path', type=str, default='./log/', help='The path to store training log and checkpoint files.')
    parser.add_argument('-d', '--data_type', choices=['RGB', 'Pencil', 'Both'], default='Pencil',
                        help='The data type that be used for training.')
    parser.add_argument('-s', '--sub_mean', default=False, action='store_true', help='Switch to subtract the mean.')

    parser.add_argument('--root_dir', default='./data')
    parser.add_argument('--text_file', default='small_text.txt')
    parser.add_argument('--img_size', type=int, nargs='+', default=[448, 448])

    # parser.add_argument('--ckpt_path', type=str, default=None)
    parser.add_argument('--ckpt_path', type=str, default='/cdengdata/posenet/iron_aug_old_loss_Pencil/PoseNet.ckpt_49999_27.533')
    parser.add_argument('--batch_size', type=int, default=30)

    args = parser.parse_args()

    ckpt_file = args.ckpt_path
    test_data = DataSource(args, text_file=args.text_file)

    channel_dict = {'Both': 4, 'Pencil': 1, 'RGB': 3}
    in_shape = [None] + args.img_size + [channel_dict[args.data_type]]

    images  = tf.placeholder(tf.float32, in_shape)
    net = PoseNet({'data': images})
    p3_x = net.layers['cls1_fc_pose_xyz']
    p3_q = net.layers['cls1_fc_pose_wpqr']

    saver = tf.train.Saver()
    gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.8)

    results = np.zeros((len(test_data), 2))
    with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:

        try:
            saver.restore(sess, ckpt_file)
        except:
            print('>>> Warning: Please check ckpt path correctness.')

        for i, (np_image, pose_x, pose_q) in enumerate(test_data):
            feed = {images: [np_image]}

            predicted_x, predicted_q = sess.run([p3_x, p3_q], feed_dict=feed)

            pose_q = np.squeeze(pose_q)
            pose_x = np.squeeze(pose_x)
            predicted_q = np.squeeze(predicted_q)
            predicted_x = np.squeeze(predicted_x)

            # Compute Individual Sample Error
            q1 = pose_q / np.linalg.norm(pose_q)
            q2 = predicted_q / np.linalg.norm(predicted_q)
            d = abs(np.sum(np.multiply(q1,q2)))
            theta = 2 * np.arccos(d) * 180/math.pi
            error_x = np.linalg.norm(pose_x-predicted_x)
            results[i,:] = [error_x,theta]
            print('Iteration: {} Error XYZ (m): {} Error Q (degrees): {}'.format(i, error_x, theta) )
            print('Iteration: {} Error XYZ (m): {} Error Q (quat): {}'.format(i, error_x, d))

    median_result = np.median(results, axis=0)
    print('Median error ', median_result[0], 'm  and ', median_result[1], 'degrees.')


if __name__ == '__main__':
    main()
